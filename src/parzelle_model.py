from eigentuemer_model import Eigentuemer_Model
from datetime import datetime

class Parzelle_Model:
    '''
    Klassse repräsentiert eine Parzelle.
    '''

    def __init__(self, raw_gebaeudeinfo_list: list, stockwerkeigentum: bool):
        self.art = None
        self.sektion = None
        self.parzelle = None
        self.egrid = None
        self.flaeche = None
        self.adressen_liste = []
        self.datum = None
        self.stockwerkeigentum = stockwerkeigentum
        self.parse_raw_parzelle(raw_gebaeudeinfo_list)

    def get_art(self) -> str:
        return self.art

    def set_art(self, value: str):
        self.art = value

    def get_egrid(self) -> str:
        return self.egrid

    def set_egrid(self, value: str):
        self.egrid = value

    def parse_raw_parzelle(self, raw_gebaeudeinfo_list: list):
        if (len(raw_gebaeudeinfo_list) < 5):
            raise ValueError(f"Gebaeudeinfo with invalid format: {raw_gebaeudeinfo_list}")

        self.__parse_art(raw_gebaeudeinfo_list)
        self.__parse_sektion_parzelle(raw_gebaeudeinfo_list)
        self.__parse_egrid(raw_gebaeudeinfo_list)
        self.__parse_flaeche(raw_gebaeudeinfo_list)
        self.__parse_adressen(raw_gebaeudeinfo_list)

    def print(self):
        print("Parzelle:")
        print("-----------")
        print(f"Art:                {self.art}")
        print(f"Sektion / Parzelle: {self.sektion} / {self.parzelle}")
        print(f"E-GRID:             {self.egrid}")
        print(f"Fläche [m²]:        {self.flaeche}")
        print(f"Adressen:           {self.adressen_liste}")
        print(f"Datum:              {self.datum}")
        print(f"Stockwerkeigentum:  {'Ja' if self.stockwerkeigentum else 'Nein'}")
        print("-----------")
        print("")

    def __parse_art(self, raw_list: list[list[str]]):
        self.art = raw_list[0][1]

    def __parse_sektion_parzelle(self, raw_list: list[list[str]]):
        sektion_parzelle_vector = raw_list[1][1].split(' / ')
        if (len(sektion_parzelle_vector) != 2):
            raise ValueError(f"Sektion/Parzelle with invalid format: {raw_list}")
        self.sektion = sektion_parzelle_vector[0]
        self.parzelle = sektion_parzelle_vector[1]

    def __parse_egrid(self, raw_list: list[list[str]]):
        self.egrid = raw_list[2][1]

    def __parse_flaeche(self, raw_list: list[list[str]]):
        self.flaeche = raw_list[3][1].strip(' m²')

    def __parse_adressen(self, raw_list: list[list[str]]):
        for address in raw_list[4:]:
            self.adressen_liste.append(address[1])

class Parzelle:
    '''
    Klassse repräsentiert eine Parzelle und deren Eigentümer (1:n).
    '''

    def __init__(self, raw_grundstueckinfo_list: list[list[str]], stockwerkeigentum: bool):
        self.parzelle = None
        self.eigentuemer_liste = []
        self.parzelle = Parzelle_Model(raw_grundstueckinfo_list, stockwerkeigentum)

    def add_eigentuemer(self, eigentuemer: Eigentuemer_Model):
        self.eigentuemer_liste.append(eigentuemer)

    def set_eigentuemer(self, eigentuemer: list[Eigentuemer_Model]):
        self.eigentuemer_liste = eigentuemer

    def set_timestamp(self, timestamp: datetime):
        self.parzelle.datum = timestamp

    def get_timestamp(self) -> datetime:
        return self.parzelle.datum

    def print(self):
        self.parzelle.print()
        for count, eigentuemer in enumerate(self.eigentuemer_liste):
            print(f"Eigentuemer {count + 1}/{len(self.eigentuemer_liste)}")
            eigentuemer.print()

    def get_egrid(self) -> str:
        return self.parzelle.get_egrid()
