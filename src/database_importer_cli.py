import fnmatch
import inquirer
from sfa_atlas_adapter import legacy_sfa_atlas_db_adapter, PostgresDatabase
import os
from dotenv import load_dotenv
from eigentumsauskunft_parser import eigentum_parser

def validate_pdf_path(path: str) -> list[str]:
    result = []
    if (os.path.isfile(path)):
        if fnmatch.fnmatch(path, "*.pdf"):
            result.append(path)
    elif (os.path.isdir(path)):
        for root, dirs, files in os.walk(path):
            for file in files:
                if fnmatch.fnmatch(file, "*.pdf"):
                    result.append(os.path.join(root, file))
    else:
        return None
    return result

def main():
    print("=============SFA PDF to database==============")
    print("Load .env")
    load_dotenv()
    print("Where are the PDFs you want to read in?")
    query_pdf_location = [
        inquirer.Path("pdf_dir", message="File or Directory", exists=True, default="testdaten/pdf")
    ]
    given_pdf_location = inquirer.prompt(query_pdf_location)
    files_to_parse = validate_pdf_path(given_pdf_location['pdf_dir'])
    if (files_to_parse == None):
        raise ValueError(f"Invalid file or directory: {given_pdf_location['pdf_dir']}")

    print("Which database should be used for import?")
    query_database = [
        inquirer.Text("name", message="DB name", default=os.getenv("SFA_LEGACY_DB_NAME")),
        inquirer.Text("host", message="Host", default=os.getenv("SFA_LEGACY_DB_HOST")),
        inquirer.Text("port", message="Port", default=os.getenv("SFA_LEGACY_DB_PORT")),
        inquirer.Text("user", message="User", default=os.getenv("SFA_LEGACY_DB_USER")),
        inquirer.Text("password", message="Password", default=os.getenv("SFA_LEGACY_DB_PASS"))
    ]

    db_data = inquirer.prompt(query_database)
    db_initializer = PostgresDatabase(db_data)
    db_adapter = legacy_sfa_atlas_db_adapter(db_initializer)

    for index, file in enumerate(files_to_parse):
        print(f"Lese Datei {index + 1}/{len(files_to_parse)}")
        auskunft = eigentum_parser(file)
        db_adapter.try_import(auskunft.get_parzelle())

    print(f"{len(files_to_parse)} Dateien gescannt.")
    db_adapter.print_meta_data()

if __name__ == "__main__":
    main()
