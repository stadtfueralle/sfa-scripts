from py_pdf_parser.loaders import load_file
from py_pdf_parser.visualise import visualise
from py_pdf_parser import tables
import argparse
import os
import fnmatch
from datetime import datetime
from eigentuemer_model import Eigentuemer_Model
from parzelle_model import Parzelle
import locale

class Eigentumsauskunft:
    """
    
    """

    FONT_MAPPING = {
        r"\w{6}\+DejaVuSans-Bold,10.0": "daten_bold",
        r"\w{6}\+DejaVuSans,6.0": "footer",
        r"\w{6}\+DejaVuSans-Bold,8.0": "titel",
        r"\w{6}\+DejaVuSans,10.0": "daten",
        r"\w{6}\+DejaVuSans-Bold,12.0": "sub_titel"
    }

    def __init__(self, filename: str) -> None:
        self.__document = None
        self.__filename = filename

        self.parzelle = None
        self.__grundstuecksangaben_tabelle = None
        self.__eigentuemer = None
        self.__eigentuemer_tabelle = []
        self.load()

    def get_street(self) -> str:
        return self.__grundstuecksangaben_tabelle[4][1]

    def get_parzelle(self) -> Parzelle:
        return self.parzelle

    def get_raw_eigentuemer_table(self) -> list[list]:
        return self.__eigentuemer_tabelle

    def get_raw_grundstueck_table(self) -> list[list]:
        return self.__grundstuecksangaben_tabelle

    def show(self):
        self.parzelle.print()

    def __format_gebaeude_adresse(self, adresse: str) -> str:
        adresse = adresse.replace('" null', '')
        adresse = adresse.replace('"', '')
        return adresse

    def __get_gebaeude_adressen_offset(self) -> int:
        for idx, titel in enumerate(self.__grundstuecksangaben_tabelle):
            if titel[0] == "Gebäudeadressen:":
                return idx

    def load(self):
        print("-----------------------------------------------")
        print(f"Datei: {self.__filename}")
        print(" ")
        self.__document = load_file(self.__filename, font_mapping=Eigentumsauskunft.FONT_MAPPING, font_mapping_is_regex=True)

        #for font in self.__document.fonts:
        #    print(f"============{font}=====================")
        #    for element in self.__document.elements.filter_by_font(font):
        #        print(element.text())

        grundstuecksangaben_element = (
            self.__document.elements.filter_by_font("sub_titel")
            .filter_by_text_equal("Grundstücksangaben:")
            .extract_single_element()
        )

        eigentum_element = (
            self.__document.elements.filter_by_font("sub_titel")
            .filter_by_text_equal("Eigentum:")
            .extract_single_element()
        )

        hinweis_element = (
            self.__document.elements.filter_by_font("sub_titel")
            .filter_by_text_equal("Hinweis:")
            .extract_single_element()
        )

        grundstuecksangaben_section = self.__document.sectioning.create_section(
            name="grundstuecksangaben",
            start_element=grundstuecksangaben_element,
            end_element=eigentum_element,
            include_last_element=False
        )

        eigentum_section = self.__document.sectioning.create_section(
            name="eigentum",
            start_element=eigentum_element,
            end_element=hinweis_element,
            include_last_element=False
        )

        self.__grundstuecksangaben_tabelle = tables.extract_table(
            grundstuecksangaben_section.elements.filter_by_fonts("daten_bold", "daten"),
            as_text=True,
            fix_element_in_multiple_rows=True
        )

        stockwerkeigentum = len(
            self.__document.elements
                .filter_by_font("daten")
                .filter_by_text_equal("Auf dem Grundstück besteht Stockwerkeigentum!")
            ) > 0

        gebäudeAdressenOffset = self.__get_gebaeude_adressen_offset()
        for i in range(gebäudeAdressenOffset, len(self.__grundstuecksangaben_tabelle)):
            if self.__grundstuecksangaben_tabelle[i][0] == '':
                self.__grundstuecksangaben_tabelle[i][1] = self.__format_gebaeude_adresse(self.__grundstuecksangaben_tabelle[i][1])

        self.parzelle = Parzelle(self.__grundstuecksangaben_tabelle, stockwerkeigentum)

        self.__eigentuemer = tables.extract_simple_table(
            eigentum_section.elements.filter_by_fonts("daten")
            .filter_by_text_contains("- "),
            as_text=True
        )

        for eigentuemer_string in self.__eigentuemer:
            self.parzelle.add_eigentuemer(Eigentuemer_Model(eigentuemer_string))

        doc_time = tables.extract_simple_table(
            self.__document.elements.filter_by_font("footer")
            .filter_by_text_contains("Druckdatum"),
            as_text=True
        )[0][0]

        # timestring parsing only works if locale explicitly set to german
        timestring = doc_time.strip('Druckdatum ')
        locale.setlocale(locale.LC_ALL, "de_DE")
        timestamp = datetime.strptime(timestring, "%d. %B %Y %H:%M")
        self.parzelle.set_timestamp(timestamp)

def eigentum_parser(filename: str) -> Eigentumsauskunft:
    return Eigentumsauskunft(filename=filename)

def create_file_name(auskunft: Eigentumsauskunft, current_directory: str) -> str:
    name = auskunft.get_street()
    if (name == "nicht vorhanden"):
        name = auskunft.get_parzelle().get_egrid()
    name = name + ".pdf"
    return os.path.join(current_directory, name)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, action="store", required=True, help="PDF file or path to folder with PDF files to import.")
    parser.add_argument("-r", "--rename", action="store_true", default=False, help="Rename PDF file to first adress found.")
    args = parser.parse_args()

    if (os.path.isfile(args.file)):
        auskunft = eigentum_parser(args.file)
        auskunft.show()

        if (auskunft.get_street() != "" and args.rename == True):
            current_directory = os.path.dirname(args.file)
            new_file = create_file_name(auskunft, current_directory)
            os.rename(args.file, new_file)
    elif (os.path.isdir(args.file)):
        for file in os.listdir(args.file):
            if fnmatch.fnmatch(file, "*.pdf"):
                auskunft = eigentum_parser(os.path.join(args.file, file))
                auskunft.show()

                if (auskunft.get_street() != "" and args.rename == True):
                    current_directory = args.file
                    new_file = create_file_name(auskunft, current_directory)
                    os.rename(os.path.join(args.file, file), new_file)
    else:
        raise FileExistsError(f"Given file or folder not found! Given: {args.file}")
