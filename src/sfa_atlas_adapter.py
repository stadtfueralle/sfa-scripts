import psycopg2
from eigentuemer_model import Eigentuemer_Model
from datetime import datetime
from parzelle_model import Parzelle

class PostgresDatabase:
    def __init__(self, name: str, user: str, password: str, host: str, port: str):
        self.name = name
        self.user = user
        self.password = password
        self.host = host
        self.port = port

    def __init__(self, input_dict):
        self.name = input_dict['name']
        self.user = input_dict['user']
        self.password = input_dict['password']
        self.host = input_dict['host']
        self.port = input_dict['port']

class legacy_sfa_atlas_db_adapter:
    """
    This class is used as database adapter between the old sfa-legacy database
    and the data parsed from the Eigentumsauskunft PDF files.
    """
    def __init__(self, db_initializer: PostgresDatabase):
        self.name = db_initializer.name
        self.user = db_initializer.user
        self.password = db_initializer.password
        self.host = db_initializer.host
        self.port = db_initializer.port
        self.connection = psycopg2.connect(f"dbname={self.name} user={self.user} password={self.password} host={self.host} port={self.port}")

        self.new_eigentuemer_list = []
        self.new_eigentum_list = []

    #
    # pulic
    #
    def print_meta_data(self):
        print(f"{len(self.new_eigentuemer_list)} Besitzende hinzugefügt.")
        print(f"{len(self.new_eigentum_list)} Eigentumsverhältnisse hinzugefügt.")

    def try_import(self, eigentumsauskunft: Parzelle):
        # in general, create n x m entries in tab_eigentum,
        # wheren n is the number of adresses within the corresponding parzelle,
        # and m is the number of owner related to the parzelle.
        # This means, for adress in adress_list and for each owner in owner_list, create one entry in tab_eigentum
        for adress in eigentumsauskunft.parzelle.adressen_liste:
            strassenr_id = self._get_or_add_adress_id(adress)
            print(f"Found id {strassenr_id}")
            for eigentuemer in eigentumsauskunft.eigentuemer_liste:
                eigentuemer_id = self._get_or_add_eigentuemer_id(eigentuemer)
                print(f"Found id {eigentuemer_id}")
                eigentum_id = self._get_or_add_eigentum(strassenr_id, eigentuemer_id, eigentumsauskunft.parzelle.datum)
                print(f"Eigentum ID: {eigentum_id}")

    #
    # private
    #
    def _build_adress_lookup_str(self, column_name: str, adress: str) -> str:
        return f"REPLACE({column_name}, ' ', '') LIKE REPLACE('{adress}', ' ', '')"

    def _build_nice_adress(self, adress: str) -> str:
        split_adress = adress.split(" ")
        result = ""
        for part in split_adress:
            result += part.strip(" ") + " "
        result = result.strip(" ")
        return f"'{result}'"

    def _sql_find_unique_id(self, query: str) -> int:
        with self.connection.cursor() as cur:
            cur.execute(query)
            result = cur.fetchall()
            if (len(result) == 0):
                return []
            else:
                return result[0]

    def _sql_insert_and_return_id(self, query: str) -> int:
        with self.connection.cursor() as cur:
            cur.execute(query)
            new_id = cur.fetchone()[0]
            self.connection.commit()
            return new_id

    def _get_or_add_adress_id(self, adress: str) -> int:
        print(f"Look up strasse_id for {adress}")
        adress_lookup = self._build_adress_lookup_str("strasse_strasse_nr", adress)
        query = f"SELECT strasse_id \
                FROM tab_strassenr \
                WHERE {adress_lookup}"
        result = self._sql_find_unique_id(query)
        if (len(result) == 1):
            return result[0]
        elif (len(result) > 1):
            ValueError(f"Duplicate for {adress}")
        nice_adress = self._build_nice_adress(adress)
        insert_query = f"INSERT INTO tab_strassenr (strasse_strasse_nr) \
                        VALUES ({nice_adress}) \
                        RETURNING strasse_id"
        return self._sql_insert_and_return_id(insert_query)

    def _get_or_add_eigentuemer_id(self, eigentuemer: Eigentuemer_Model) -> int:
        print(f"Look up eigentuemer_id for {eigentuemer.name}")
        adress_lookup = self._build_adress_lookup_str("eigentuemer_strasse_nr", eigentuemer.strasse_nr)
        query = f"SELECT eigentuemer_id \
                FROM htab_eigentuemer \
                WHERE {adress_lookup} \
                AND eigentuemer_plz = '{eigentuemer.plz}' \
                AND eigentuemer_ort = '{eigentuemer.city}' \
                AND eigentuemer_land = '{eigentuemer.country}'"
        result = self._sql_find_unique_id(query)
        if (len(result) == 1):
            return result[0]
        elif (len(result) > 1):
            ValueError(f"Duplicate for {eigentuemer.name}")
        import_marker = "script_import" # kann leer sein
        insert_query = f"INSERT INTO htab_eigentuemer (eigentuemer_name, eigentuemer_strasse_nr, eigentuemer_plz, eigentuemer_ort, eigentuemer_bemerkung, eigentuemer_land) \
                        VALUES ('{eigentuemer.name}', '{eigentuemer.strasse_nr}', '{eigentuemer.plz}', '{eigentuemer.city}', '{import_marker}', '{eigentuemer.country}') \
                        RETURNING eigentuemer_id"
        new_id = self._sql_insert_and_return_id(insert_query)
        self.new_eigentuemer_list.append(new_id)
        return new_id

    def _get_or_add_eigentum(self, strasse_id: int, eigentuemer_id: int, datum: datetime) -> int:
        print(f"Look up eigentum_id for strasse_id {strasse_id} & eigentuemer_id {eigentuemer_id}")
        query = f"SELECT eigen_id \
                FROM tab_eigentum \
                WHERE eigen_idstrasse = '{strasse_id}' \
                AND eigen_ideigentuemer = '{eigentuemer_id}'"
        result = self._sql_find_unique_id(query)
        if (len(result) == 1):
            return result[0]
        elif (len(result) > 1):
            ValueError(f"Duplicate detected")
        else:
            import_marker = "script_import"
            eigentum_pdf_import_bemerkung = "geomaps" # stockwerkeigentum
            insert_query = f"INSERT INTO tab_eigentum (eigen_idstrasse, eigen_datum, eigen_besitz, eigen_bemerkung, eigen_ideigentuemer) \
                            VALUES ('{strasse_id}', '{datum}', '{import_marker}', '{eigentum_pdf_import_bemerkung}', '{eigentuemer_id}') \
                            RETURNING eigen_id"
            new_id = self._sql_insert_and_return_id(insert_query)
            self.new_eigentum_list.append(new_id)
            return new_id
