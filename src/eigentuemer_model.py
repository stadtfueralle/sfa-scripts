import inquirer
import string
import re

class Eigentuemer_Model:
    '''
    Klassse repräsentiert einen Eigentümer.
    '''
    def __init__(self, raw_eigentuemer_list: list[str]):
        self.name = None
        self.strasse_nr = None
        self.plz = None
        self.city = None
        self.country = None
        self.current_owner = ""
        self.parse_raw_owner(raw_eigentuemer_list)

    def parse_raw_owner(self, raw_eigentuemer_list: list[str]):
        self.current_owner = raw_eigentuemer_list
        eigentuemer = raw_eigentuemer_list[0].split(",")
        try:
            if (self.__try_parse_australian_address(eigentuemer)):
                # do nothing, australian address parsed
                pass
            elif (self.__try_parse_default_address(eigentuemer)):
                # do nothing, default address parsed
                pass
            elif (self.__try_parse_french_address(eigentuemer)):
                # do nothing, french address parsed
                pass
            elif (self.__try_parse_address_with_flat_number(eigentuemer)):
                # do nothing, adress with flat number parsed
                pass
            elif (self.__cli_input_query_for_address()):
                # do nothing, address entered manually
                pass
            else:
                raise ValueError(f"Was not able to parse Eigentuemer: {eigentuemer}")
        except:
            if (self.__cli_input_query_for_address()):
                # do nothing, address entered manually
                pass
            else:
                raise ValueError(f"Was not able to parse Eigentuemer: {eigentuemer}")
        self.__normalize_input_for_sql()

    def __default_array_to_model(self, properties: list[str]) -> bool:
        if (self.__parse_name(properties) == False):
            return False
        if (self.__parse_strasse_nr(properties) == False):
            return False
        if (self.__parse_plz_city_country(properties) == False):
            return False
        return True

    def print(self):
        print("-----------")
        print(f"Name:         {self.name}")
        print(f"Strasse / NR: {self.strasse_nr}")
        print(f"PLZ / Stadt:  {self.plz} {self.city}")
        print(f"Land:         {self.country}")
        print("-----------")
        print("")

    def __normalize_input_for_sql(self):
        if self.name:
            self.name = self.name.replace("'", "''")
        if self.city:
            self.city = self.city.replace("'", "''")
        if self.country:
            self.country = self.country.replace("'", "''")
        if self.strasse_nr:
            self.strasse_nr = self.strasse_nr.replace("'", "''")

    def __try_parse_french_address(self, adresse: list[str]) -> bool:
        if (len(adresse) == 4):
            hausnummer = adresse[1].strip(' ')
            if (self.__is_house_number(hausnummer)):
                self.__default_array_to_model([adresse[0].strip(' -'), hausnummer + adresse[2], adresse[3].strip(' ')])
                return True
        return False

    def __try_parse_address_with_flat_number(self, adresse: list[str]) -> bool:
        if (len(adresse) == 4):
            wohnungsnummer = adresse[2].strip(' ')
            strasse_array = adresse[1].split(' ')
            if (len(wohnungsnummer) <= 2 and strasse_array[-1].strip(' ').isnumeric()):
                self.__default_array_to_model([adresse[0].strip(' -'), adresse[1].strip(' '), adresse[3].strip(' ')])
                return True
        return False

    def __try_parse_australian_address(self, adresse: list[str]) -> bool:
        if (len(adresse) == 3):
            country = adresse[2].split('-')[0].strip(' ')
            if (country != 'AU'):
                return False

            city_plz = adresse[2].split('-')[1].strip(' ').split(' ')
            if (len(city_plz) != 3):
                return False

            self.__default_array_to_model([adresse[0].strip(' -'), adresse[1].strip(' '), country + '-' + city_plz[2] + ' ' + city_plz[0] + '-' + city_plz[1]])
            return True

        return False

    def __try_parse_default_address(self, adresse: list[str]) -> bool:
        if (len(adresse) == 3):
            adresse = [property.strip(" -") for property in adresse]
            return self.__default_array_to_model(adresse)
        elif (len(adresse) == 2):
            return self.__default_array_to_model([adresse[0].strip(" -"), '', adresse[1].strip(' ')])
        elif (len(adresse) == 1):
            adresse = adresse[0].strip(" -")
            return self.__default_array_to_model([adresse[0].strip(" -"), '', ''])
        return False

    def __is_house_number(self, value: str) -> bool:
        if (value.isnumeric()):
            return True
        if any([value.endswith(letter) for letter in list(string.ascii_lowercase)]):
            return True
        return False

    def __parse_name(self, raw_list: list[str]) -> bool:
        self.name = raw_list[0]
        return True

    def __parse_strasse_nr(self, raw_list: list[str]) -> bool:
        self.strasse_nr = raw_list[1]
        return True

    def __parse_plz_city_country(self, raw_list: list[str]) -> bool:
        if (raw_list[2] == ''):
            # no plz, city, country found
            self.plz = ''
            self.city = ''
            self.country = ''
            return True

        splitted_plz_city = raw_list[2].split(" ")
        if len(splitted_plz_city) >= 1:
            swiss_ctry_plz_pattern = r"^[A-Z]{2}-\d{4}$" # regex pattern matching CH-1234 (country-plz)
            portuguese_ctry_plz_pattern = r"^[A-Z]{2}-\d{4}-\d{3}$" # regex pattern matching PT-1234-567 (country-plz)
            usa_ctry_plz_pattern = r"^[A-Z]{2}-\d{5}$" # regex pattern matching US-12345 (country-plz)
            if re.match(swiss_ctry_plz_pattern, splitted_plz_city[0]) or re.match(usa_ctry_plz_pattern, splitted_plz_city[0]):
                [self.country, self.plz] = splitted_plz_city[0].split("-")
                self.city = " ".join(splitted_plz_city[1:])
                return True
            elif re.match(portuguese_ctry_plz_pattern, splitted_plz_city[0]):
                [self.country, self.plz] = [splitted_plz_city[0].split("-")[0], "-".join(splitted_plz_city[0].split("-")[1:])]
                self.city = " ".join(splitted_plz_city[1:])
                return True

        if (len(splitted_plz_city) != 2):
            return False

        if (len(splitted_plz_city) == 3):
            # we have a canadian adress
            self.city = splitted_plz_city[1].strip(' ') + " - " + splitted_plz_city[2].strip(' ')
            self.plz = ""
            self.country = splitted_plz_city[0].strip('-')
        else:
            self.city = splitted_plz_city[1]
            splitted_plz_country = splitted_plz_city[0].split("-")
            if (len(splitted_plz_country) != 2):
                raise ValueError(f"Adress with invalid format: {raw_list}")
            self.country = splitted_plz_country[0]
            self.plz = splitted_plz_country[1]
        return True

    def __cli_input_query_for_address(self) -> bool:
        query_address_fields = [
            inquirer.Text("name", message=f"Name"),
            inquirer.Text("strasse_nr", message=f"Strasse Nr"),
            inquirer.Text("plz", message=f"PLZ"),
            inquirer.Text("city", message=f"Stadt"),
            inquirer.Text("country", message=f"Ländercode (z.B: CH, DE, ...)")
        ]

        print(f"Eigentümer konnte nicht eingelesen werden, bitte Daten manuell eingeben:\n\n{self.current_owner}\n")
        address_fields = inquirer.prompt(query_address_fields)
        self.name = address_fields['name']
        self.strasse_nr = address_fields['strasse_nr']
        self.plz = address_fields['plz']
        self.city = address_fields['city']
        self.country = address_fields['country']
        return True
