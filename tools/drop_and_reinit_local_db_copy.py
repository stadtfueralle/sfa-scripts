import subprocess
import os
from dotenv import load_dotenv

def drop_database(db_name: str, user: str, password: str, host: str, port: str, force=False):
    """Drops a PostgreSQL database using the dropdb command."""

    print("Drop DB ...")

    env = os.environ.copy()
    env["PGPASSWORD"] = password

    # Construct the dropdb command
    command = ["dropdb", "-U", user, "-h", host, "-p", port]

    # Add --force option if specified
    if force:
        command.append("--force")

    # Add the database name
    command.append(db_name)

    try:
        # Execute the command
        subprocess.run(command, env=env, check=True)
        print(f"Database '{db_name}' dropped successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Error dropping database '{db_name}': {e}")
        exit(1)


def create_database(db_name: str, user: str, password: str, host: str, port: str):
    """Creates a PostgreSQL database using the createdb command."""

    print("Create DB ...")

    env = os.environ.copy()
    env["PGPASSWORD"] = password

    # Construct the dropdb command
    command = ["createdb", "-U", user, "-h", host, "-p", port, db_name]

    try:
        # Execute the command
        subprocess.run(command, env=env, check=True)
        print(f"Database '{db_name}' created successfully.")
        return True
    except subprocess.CalledProcessError as e:
        print(f"Error creating database '{db_name}': {e}")
        exit(1)

def import_database_dump(db_name: str, user: str, password: str, host: str, port: str, db_dump_path: str):
    """Imports a PostgreSQL database dump using the psql command line."""

    print("Import DB ...")

    env = os.environ.copy()
    env["PGPASSWORD"] = password

    # Construct the dropdb command
    command = ["psql", "-U", user, "-h", host, "-p", port, "-d", db_name, "-f", db_dump_path]

    try:
        # Execute the command
        subprocess.run(command, env=env, check=True)
        print(f"Dump '{db_dump_path}' imported successfully.")
        return True
    except subprocess.CalledProcessError as e:
        print(f"Error importing database '{db_dump_path}': {e}")
        exit(1)


# Example Usage
if __name__ == "__main__":
    print("===========")
    print("Load .env")
    load_dotenv()

    db_name = os.getenv("SFA_LEGACY_DB_NAME")
    db_user = os.getenv("SFA_LEGACY_DB_USER")
    db_pass = os.getenv("SFA_LEGACY_DB_PASS")
    db_host = os.getenv("SFA_LEGACY_DB_HOST")
    db_port = os.getenv("SFA_LEGACY_DB_PORT")
    db_dump = os.getenv("SFA_LEGACY_DB_DUMP")

    drop_database(db_name, db_user, db_pass, db_host, db_port)
    create_database(db_name, db_user, db_pass, db_host, db_port)
    import_database_dump(db_name, db_user, db_pass, db_host, db_port, db_dump)
