from py_pdf_parser.loaders import load_file
from py_pdf_parser.visualise import visualise
from py_pdf_parser import tables
import argparse
import os
import fnmatch
from tabulate import tabulate

class Eigentumsauskunft:
    """
    
    """

    FONT_MAPPING = {
        r"\w{6}\+DejaVuSans-Bold,10.0": "daten_bold",
        r"\w{6}\+DejaVuSans,6.0": "footer",
        r"\w{6}\+DejaVuSans-Bold,8.0": "titel",
        r"\w{6}\+DejaVuSans,10.0": "daten",
        r"\w{6}\+DejaVuSans-Bold,12.0": "sub_titel"
    }

    def __init__(self, filename: str) -> None:
        self.__document = None
        self.__filename = filename

        self.__grundstuecksangaben_tabelle = None
        self.__eigentuemer = None
        self.__eigentuemer_tabelle = []
        self.load()

    def show(self):
        print("----------------------Grundstücksangaben----------------------")
        print(tabulate(self.__grundstuecksangaben_tabelle))
        print("")
        print("----------------------Eigentümer----------------------")
        print(tabulate(self.__eigentuemer_tabelle))

    def load(self):
        print(" ")
        print(" ")
        print(" ")
        print(f"Parsing file: {self.__filename}")
        print(" ")
        self.__document = load_file(self.__filename, font_mapping=Eigentumsauskunft.FONT_MAPPING, font_mapping_is_regex=True)
        
        #for font in self.__document.fonts:
        #    print(f"============{font}=====================")
        #    for element in self.__document.elements.filter_by_font(font):
        #        print(element.text())

        grundstuecksangaben_element = (
            self.__document.elements.filter_by_font("sub_titel")
            .filter_by_text_equal("Grundstücksangaben:")
            .extract_single_element()
        )

        eigentum_element = (
            self.__document.elements.filter_by_font("sub_titel")
            .filter_by_text_equal("Eigentum:")
            .extract_single_element()
        )

        hinweis_element = (
            self.__document.elements.filter_by_font("sub_titel")
            .filter_by_text_equal("Hinweis:")
            .extract_single_element()
        )

        grundstuecksangaben_section = self.__document.sectioning.create_section(
            name="grundstuecksangaben",
            start_element=grundstuecksangaben_element,
            end_element=eigentum_element,
            include_last_element=False
        )

        eigentum_section = self.__document.sectioning.create_section(
            name="eigentum",
            start_element=eigentum_element,
            end_element=hinweis_element,
            include_last_element=False
        )

        self.__grundstuecksangaben_tabelle = tables.extract_table(
            grundstuecksangaben_section.elements.filter_by_fonts("daten_bold", "daten"),
            as_text=True,
            fix_element_in_multiple_rows=True
        )

        self.__eigentuemer = tables.extract_simple_table(
            eigentum_section.elements.filter_by_fonts("daten")
            .filter_by_text_contains("- "),
            as_text=True
        )

        for eigentuemer_string in self.__eigentuemer:
            eigentuemer = eigentuemer_string[0].split(",")
            if (len(eigentuemer) == 3):
                eigentuemer = [property.strip(" -") for property in eigentuemer]
                self.__eigentuemer_tabelle.append(eigentuemer)
            elif (len(eigentuemer) == 1):
                eigentuemer = eigentuemer[0].strip(" -")
                self.__eigentuemer_tabelle.append([eigentuemer, '', ''])
            else:
                raise ValueError(f"Was not able to parse Eigentuemer: {eigentuemer}")

def eigentum_parser(filename: str) -> Eigentumsauskunft:
    return Eigentumsauskunft(filename=filename)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", type=str, action="store", required=True, help="PDF file to import.")
    args = parser.parse_args()

    if (os.path.isfile(args.filename)):
        eigentum_parser(args.filename).show()
    elif (os.path.isdir(args.filename)):
        for file in os.listdir(args.filename):
            if fnmatch.fnmatch(file, "*.pdf"):
                eigentum_parser(os.path.join(args.filename,file)).show()
    else:
        raise FileExistsError(f"Given file or folder not found! Given: {args.filename}")
